library ieee;
	use ieee.std_logic_1164.all;
	
package myfunc_pkg is
	function xor_reduce(data : std_logic_vector) return std_logic;
	function xor_recursive_reduce(data : std_logic_vector) return std_logic;
end;

package body myfunc_pkg is

	function xor_reduce(data : std_logic_vector) return std_logic is
		variable xor_status : std_logic := '0';
		variable data_v		: std_logic_vector(data'range);

	
	begin
		data_v := data;

		for I in data_v'range loop	  
			xor_status := xor_status xor data_v(I);
		end loop;	
	
		return xor_status;
	end function;

	
	function xor_recursive_reduce(data : std_logic_vector) return std_logic is
				variable result		: std_logic := '0';
				variable upper, lower : std_logic;
				variable half 		: integer;
				variable data_v		: std_logic_vector(data'range);
	begin
		if( data'length < 1 ) then
			result := '0';
		else 
			data_v := data;
			if( data_v'length = 1 ) then
				result := data_v(data_v'left);
			elsif( data_v'length = 2 ) then
				result := data_v(data_v'left) xor data_v(data_v'right);
			else 
				half := (data_v'length + 1) / 2;
				if( data_v'ascending ) then
					half := half + data_v'left;
					upper := xor_recursive_reduce( data_v( data_v'left to half-1 ));
					lower := xor_recursive_reduce( data_v( half to data_v'right));
				else
					half := half + data_v'right;
					upper := xor_recursive_reduce( data_v( data_v'left downto half ));
					lower := xor_recursive_reduce( data_v( half-1 downto data_v'right));
				end if;
			result := upper xor lower;
			end if;
		end if;	
		return result;
	end function;

end package body;