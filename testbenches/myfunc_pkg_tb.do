onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /myfunc_pkg_tb/line__22/test_out_v_asc
add wave -noupdate /myfunc_pkg_tb/line__22/parity_asc
add wave -noupdate /myfunc_pkg_tb/line__22/parity_rec_asc
add wave -noupdate /myfunc_pkg_tb/line__22/test_out_v
add wave -noupdate /myfunc_pkg_tb/line__22/parity
add wave -noupdate /myfunc_pkg_tb/line__22/parity_rec
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {559 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 356
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {7488006 ns} {7488775 ns}
