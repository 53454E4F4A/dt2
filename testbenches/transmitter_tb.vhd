

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
 


entity transmitter_tb is    
   constant nob : natural := 16;  -- Most significant bit POsition             
end entity transmitter_tb;


architecture beh of transmitter_tb is

    
	  signal datai  : std_logic_vector(nob-1 downto 0) := (others => '0');
    signal datao  : std_logic_vector(nob downto 0) := (others => '0');
    signal clk    : std_logic;
    signal nres   : std_logic;
    
    component stimuliGen is
      
	 	  port (
		    clk	: out	std_logic;
		    res	: out	std_logic
		  );
	   end component stimuligen;
	   for all : stimuliGen use entity work.stimuliGen(stimuliGen_a);
   
   component testcase_e is
      generic (
        nob : natural := 16;  
        waittime : natural := 100  
      ); --generic
    port (
       test_out : out std_logic_vector(nob-1 downto 0)
    );
   end component testcase_e;
   for all : testcase_e use entity work.testcase_e(testcase_a); 
   
    component transmitter is                 
      generic (
        nob : natural := 16        
      ); -- generic

      port (
	     datao   	: out std_logic_vector(nob downto 0);
	     datai	   : in std_logic_vector(nob-1 downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
      ); -- port    
   end component transmitter;
   for all : transmitter use entity work.transmitter(beh);
   
   
begin
  datagen_i : testcase_e
   generic map(
    nob => nob       
  )
  port map(
    test_out => datai
  );
  transmitter_i : transmitter
  generic map(
    nob => nob
  )
    port map (
     datao => datao,
     datai => datai,
     clk => clk,
     nres => nres    
  );

  stimuli : stimuliGen
		port map(
			clk => clk,
			res => nres
		)
	;--]stimuli
end beh;
