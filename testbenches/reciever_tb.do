onerror {resume}
quietly virtual signal -install /reciever_tb { /reciever_tb/datai(16 downto 1)} datai_val
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Data in}
add wave -noupdate -radix hexadecimal -childformat {{/reciever_tb/datai_val(15) -radix decimal} {/reciever_tb/datai_val(14) -radix decimal} {/reciever_tb/datai_val(13) -radix decimal} {/reciever_tb/datai_val(12) -radix decimal} {/reciever_tb/datai_val(11) -radix decimal} {/reciever_tb/datai_val(10) -radix decimal} {/reciever_tb/datai_val(9) -radix decimal} {/reciever_tb/datai_val(8) -radix decimal} {/reciever_tb/datai_val(7) -radix decimal} {/reciever_tb/datai_val(6) -radix decimal} {/reciever_tb/datai_val(5) -radix decimal} {/reciever_tb/datai_val(4) -radix decimal} {/reciever_tb/datai_val(3) -radix decimal} {/reciever_tb/datai_val(2) -radix decimal} {/reciever_tb/datai_val(1) -radix decimal} {/reciever_tb/datai_val(0) -radix decimal}} -expand -subitemconfig {/reciever_tb/datai(16) {-radix decimal} /reciever_tb/datai(15) {-radix decimal} /reciever_tb/datai(14) {-radix decimal} /reciever_tb/datai(13) {-radix decimal} /reciever_tb/datai(12) {-radix decimal} /reciever_tb/datai(11) {-radix decimal} /reciever_tb/datai(10) {-radix decimal} /reciever_tb/datai(9) {-radix decimal} /reciever_tb/datai(8) {-radix decimal} /reciever_tb/datai(7) {-radix decimal} /reciever_tb/datai(6) {-radix decimal} /reciever_tb/datai(5) {-radix decimal} /reciever_tb/datai(4) {-radix decimal} /reciever_tb/datai(3) {-radix decimal} /reciever_tb/datai(2) {-radix decimal} /reciever_tb/datai(1) {-radix decimal}} /reciever_tb/datai_val
add wave -noupdate -divider {Parity in}
add wave -noupdate /reciever_tb/datai(0)
add wave -noupdate -divider {data out}
add wave -noupdate /reciever_tb/ok
add wave -noupdate -radix hexadecimal /reciever_tb/datao
add wave -noupdate -divider clk
add wave -noupdate /reciever_tb/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3542 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 244
configure wave -valuecolwidth 225
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2730 ns} {3780 ns}
