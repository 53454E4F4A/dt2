

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
 


entity transmissionChecker_tb is    
   constant nob : natural := 16;  -- Most significant bit POsition             
end entity transmissionChecker_tb;


architecture beh of transmissionChecker_tb is

    
   	signal datai  : std_logic_vector(nob-1 downto 0) := (others => '0');
    signal datao  : std_logic_vector(nob-1 downto 0) := (others => '0');
    
    signal clk    : std_logic;
    signal nres   : std_logic;
	signal ok	  : std_logic;
	
    
    component stimuliGen is
      
	 	  port (
		    clk	: out	std_logic;
		    res	: out	std_logic
		  );
	   end component stimuligen;
	   for all : stimuliGen use entity work.stimuliGen(stimuliGen_a);
   
   component testcase_e is
      generic (
        nob : natural := 16;  
        waittime : natural := 100  
      ); --generic
    port (
       test_out : out std_logic_vector(nob-1 downto 0)
    );
   end component testcase_e;
   for all : testcase_e use entity work.testcase_e(testcase_a); 
   
   component transmissionChecker is       
     generic (
        nob : natural := 16        
    ); -- generic
    port (
	   rxdat 	: out std_logic_vector(nob-1 downto 0);
	   ok		: out std_logic;
	   txdat	: in std_logic_vector(nob-1 downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

	--  constant nob : natural := 16;		-- Number of Bits  
  

	end component transmissionChecker;
    for all : transmissionChecker use entity work.transmissionChecker(beh); 
   
   
begin
  datagen_i : testcase_e
   generic map(
    nob => nob     
  )
  port map(
    test_out => datai
  );
  transmissionChecker_i : transmissionChecker
  generic map(
    nob => nob
  )
    port map (
     rxdat => datao,
     txdat => datai,
	 ok => ok,
     clk => clk,
     nres => nres    
  );

  stimuli : stimuliGen
		port map(
			clk => clk,
			res => nres
		)
	;--]stimuli
end beh;
