

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
 


entity reciever_tb is    
   constant nob : natural := 16;  -- Most significant bit POsition             
end entity reciever_tb;


architecture beh of reciever_tb is

    
	signal datai  : std_logic_vector(nob downto 0) := (others => '0');
    signal datao  : std_logic_vector(nob-1 downto 0) := (others => '0');
    signal clk    : std_logic;
    signal nres   : std_logic;
	signal ok	  : std_logic;
	
    
    component stimuliGen is
      
	 	  port (
		    clk	: out	std_logic;
		    res	: out	std_logic
		  );
	   end component stimuligen;
	   for all : stimuliGen use entity work.stimuliGen(stimuliGen_a);
   
   component testcase_e is
      generic (
        nob : natural := 16;  
        waittime : natural := 100  
      ); --generic
    port (
       test_out : out std_logic_vector(nob-1 downto 0)
    );
   end component testcase_e;
   for all : testcase_e use entity work.testcase_e(testcase_a); 
   
    component reciever is                 
      generic (
        nob : natural := 16        
      ); -- generic

       port (
	   datao 	: out std_logic_vector(nob-1 downto 0);
	   ok		: out std_logic;
	   datai	: in std_logic_vector(nob downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

   end component reciever;
   for all : reciever use entity work.reciever(beh);
   
   
begin
  datagen_i : testcase_e
   generic map(
    nob => nob+1     
  )
  port map(
    test_out => datai
  );
  reciever_i : reciever
  generic map(
    nob => nob
  )
    port map (
     datao => datao,
     datai => datai,
	 ok => ok,
     clk => clk,
     nres => nres    
  );

  stimuli : stimuliGen
		port map(
			clk => clk,
			res => nres
		)
	;--]stimuli
end beh;
