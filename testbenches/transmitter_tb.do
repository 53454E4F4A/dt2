onerror {resume}
quietly virtual signal -install /transmitter_tb { /transmitter_tb/datao(16 downto 1)} data
quietly virtual signal -install /transmitter_tb { /transmitter_tb/datao(16 downto 1)} dataout
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider data
add wave -noupdate /transmitter_tb/datai
add wave -noupdate /transmitter_tb/dataout
add wave -noupdate -divider parity
add wave -noupdate /transmitter_tb/datao(0)
add wave -noupdate -divider clk
add wave -noupdate /transmitter_tb/clk
add wave -noupdate /transmitter_tb/nres
add wave -noupdate /transmitter_tb/nob
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {696 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 313
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {646 ns} {1456 ns}
