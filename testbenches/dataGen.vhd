
library work;
    use work.all;
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    

entity testcase_e is
   generic (
        nob : natural := 16;  
        waittime : natural := 100  
      ); --generic
    port (
       test_out : out std_logic_vector(nob-1 downto 0)
    );
    constant nop : natural := nob * nob;  -- Number Of Permutations 
    constant waittime_c : natural := waittime;
end testcase_e;


architecture testcase_a of testcase_e is
begin
    process 
      variable test_out_v : std_logic_vector(nob-1 downto 0) := (others => '0');
	
    begin
    
  for I in nop downto 1 loop
        test_out <= test_out_v;
        test_out_v := std_logic_vector(unsigned(test_out_v) + 1);  
        wait for 100 ns;  
       
   	end loop;	
       
    end process;
end architecture;