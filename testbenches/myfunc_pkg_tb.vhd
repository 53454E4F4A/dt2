

library work;
    use work.all;
  
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.myfunc_pkg.all; 

entity myfunc_pkg_tb is        
 constant nob : natural := 16;       -- nubmer of bytes
 constant nop : natural := nob*nob;  -- number of permutations  
end entity myfunc_pkg_tb;


architecture beh of myfunc_pkg_tb is   

begin

  process 
	variable test_out_v : std_logic_vector(nob-1 downto 0) := (others => '0');
	variable test_out_v_asc : std_logic_vector(0 to nob-1) := (others => '0');
	variable parity : std_logic;
	variable parity_rec : std_logic;
	variable parity_asc : std_logic;
	variable parity_rec_asc : std_logic;
    begin
    
	for I in nop downto 1 loop	  
		  parity_asc := xor_reduce(test_out_v_asc);
		  parity_rec_asc := xor_recursive_reduce(test_out_v_asc);
		  parity := xor_reduce(test_out_v);
		  parity_rec := xor_recursive_reduce(test_out_v);
		  wait for 100 ns;  
		  test_out_v := std_logic_vector(unsigned(test_out_v) + 1);  
	    test_out_v_asc := std_logic_vector(unsigned(test_out_v_asc) + 1);  
   
       
   	end loop;	
       
    end process;
 
  
end beh;
