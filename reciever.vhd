
library work;
    use work.all;
    use work.myfunc_pkg.all; 
library ieee;
    use ieee.std_logic_1164.all;
 


entity reciever is                 
    generic (
        nob : natural := 16        
    ); -- generic

    port (
	   datao 	: out std_logic_vector(nob-1 downto 0);
	   ok		: out std_logic;
	   datai	: in std_logic_vector(nob downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

    constant mpo : natural := nob-1;  -- Most significant bit POsition


end entity reciever;


architecture beh of reciever is

    
	signal datai_cs : std_logic_vector(mpo+1 downto 0) := (others => '0');
    signal datai_ns : std_logic_vector(mpo+1 downto 0);
    signal datao_cs : std_logic_vector(mpo+1 downto 0) := (others => '0');
    signal datao_ns : std_logic_vector(mpo+1 downto 0);

begin

reg : process(datai) is
	variable data_v : std_logic_vector(mpo+1 downto 0);
begin
	data_v := datai;
	datai_ns <= data_v;
end process reg;

paritycheck : process(datai_cs) is
	variable data_v : std_logic_vector(mpo+1 downto 0);
	variable parity_v : std_logic := '0';
begin
	data_v := datai_cs;
		parity_v := xor_reduce(data_v(mpo+1 downto 1));
  --parity_v := xor_recursive_reduce(data_v(mpo+1 downto 1));
	if data_v(0) = parity_v then
		data_v(0) := '1';
	else 
		data_v(0) := '0';
	end if;
	
	datao_ns <= data_v;
	
	
end process paritycheck;

datao <= datao_cs(mpo+1 downto 1);
ok		<= datao_cs(0);




sync : process(clk) is
begin
	if rising_edge(clk) then
		if nres = '1' then 
			datao_cs <= (others => '0');		
		else		
			datai_cs <= datai_ns;
			datao_cs <= datao_ns;		 
		end if; 
	end if; 
end process sync;


end beh;