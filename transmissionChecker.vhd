
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
 


entity transmissionChecker is       
     generic (
        nob : natural := 16        
    ); -- generic
    port (
	   rxdat 	: out std_logic_vector(nob-1 downto 0);
	   ok		: out std_logic;
	   txdat	: in std_logic_vector(nob-1 downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

	--  constant nob : natural := 16;		-- Number of Bits  
    constant mpo : natural := nob-1;  -- Most significant bit POsition


end entity transmissionChecker;


architecture beh of transmissionChecker is

    
	signal data_s : std_logic_vector(nob downto 0) := (others => '0');
	
	component transmitter is                 
    generic (
        nob : natural := 16        
    ); -- generic

    port (
	   datao 	: out std_logic_vector(nob downto 0);
	   datai	: in std_logic_vector(nob-1 downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

   	end component transmitter;
	 for all : transmitter use entity work.transmitter(beh);
	component reciever is                 
    generic (
        nob : natural := 16        
    ); -- generic

    port (
	   datao 	: out std_logic_vector(nob-1 downto 0);
	   ok		: out std_logic;
	   datai	: in std_logic_vector(nob downto 0);
       clk      : in std_logic;      -- CLocK
       nres     : in std_logic       -- Synchronous RESET
    ); -- port

 

	end component reciever;
	 for all : reciever use entity work.reciever(beh);
	

begin

 transmitter_i : transmitter
  generic map(
    nob => nob
  )
    port map (
     datao => data_s,
     datai => txdat,
     clk => clk,
     nres => nres    
  );

	 reciever_i : reciever
  generic map(
    nob => nob
  )
    port map (
     datao => rxdat,
     datai => data_s,
	 ok => ok,
     clk => clk,
     nres => nres    
  );
  

end beh;